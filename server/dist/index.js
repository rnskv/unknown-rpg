"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var app = express_1.default();
var http = require("http").Server(app);
var WebSocket = require("ws");
var GameEvent;
(function (GameEvent) {
    GameEvent["LOGIN"] = "game:login";
    GameEvent["JOIN"] = "game:join";
    GameEvent["SYNC"] = "game:sync";
    GameEvent["INIT"] = "game:init";
    GameEvent["LEAVE"] = "game:leave";
})(GameEvent || (GameEvent = {}));
var PlayerEvent;
(function (PlayerEvent) {
    PlayerEvent["UPDATE"] = "player:update";
})(PlayerEvent || (PlayerEvent = {}));
var Game = /** @class */ (function () {
    function Game(_a) {
        var config = _a.config;
        this.config = {
            maxUsers: 8,
        };
        this.state = {
            players: new Map(),
        };
        this.config = __assign(__assign({}, this.config), config);
    }
    Game.prototype.start = function () {
        var _this = this;
        wss.on('connection', function (ws) {
            ws.id = wss.getUniqueID();
            ws.on('close', function () {
                _this.onLeave(ws.id);
            });
            ws.on('message', function (event) {
                var _a = JSON.parse(event), type = _a.type, data = _a.data;
                console.log('received: %s', type, data);
                switch (type) {
                    case GameEvent.JOIN: {
                        _this.onJoin({
                            ws: ws,
                            id: ws.id,
                            nickname: data.nickname || ws.id,
                            position: { x: 0, y: 0 },
                            velocity: { x: 10, y: 10 },
                        });
                        break;
                    }
                    case PlayerEvent.UPDATE: {
                        console.log('data', ws.id, data);
                        _this.onPlayerUpdate(ws.id, data);
                    }
                }
            });
        });
    };
    Game.prototype.emitAll = function (type, data) {
        var players = this.state.players;
        players.forEach(function (player) {
            var payload = JSON.stringify({
                type: type,
                data: data,
            });
            player.ws.send(payload);
        });
    };
    Game.prototype.emit = function (ws, type, data) {
        var payload = JSON.stringify({
            type: type,
            data: data,
        });
        console.log("Emit to " + ws.id + " event - " + type);
        ws.send(payload);
    };
    Game.prototype.onJoin = function (data) {
        this.state.players.set(data.id, data);
        this.emitAll(GameEvent.JOIN, data);
        this.emit(data.ws, GameEvent.LOGIN, data.id);
    };
    Game.prototype.update = function () {
        this.emitAll(GameEvent.INIT, Array.from(this.state.players.values()));
        setTimeout(this.update.bind(this), 1000 / 5);
    };
    Game.prototype.onLeave = function (id) {
        this.state.players.delete(id);
        this.emitAll(GameEvent.LEAVE, id);
    };
    Game.prototype.onMove = function () {
    };
    Game.prototype.onPlayerUpdate = function (id, data) {
        var player = this.state.players.get(id);
        if (!player) {
            return;
        }
        this.state.players.set(id, __assign(__assign({}, player), data));
    };
    return Game;
}());
app.get('/', function (req, res) {
    res.send('Unknown rpg api!!');
});
var wss = new WebSocket.Server({
    port: 8080,
    perMessageDeflate: {
        zlibDeflateOptions: {
            chunkSize: 1024,
            memLevel: 7,
            level: 3
        },
        zlibInflateOptions: {
            chunkSize: 10 * 1024
        },
        clientNoContextTakeover: true,
        serverNoContextTakeover: true,
        serverMaxWindowBits: 10,
        concurrencyLimit: 10,
        threshold: 1024
    }
});
wss.getUniqueID = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4();
};
var game = new Game({
    config: {
        maxUsers: 2,
    },
    wss: wss,
});
game.start();
game.update();
var server = http.listen(3000, function () {
    console.log("listening on *:3000");
});
