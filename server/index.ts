import express from 'express';

const app: express.Application = express();

const http = require("http").Server(app);
const WebSocket = require("ws");

enum GameEvent {
    LOGIN = 'game:login',
    JOIN = 'game:join',
    SYNC = 'game:sync',
    INIT = 'game:init',
    LEAVE = 'game:leave',
}

enum PlayerEvent {
    UPDATE = 'player:update',
}
interface IGame {
    config: {
        maxUsers: number,
    },
    wss: any,
}

interface IPlayer {
    ws: any,
    position: {
        x: number,
        y: number,
    },
    velocity: {
        x: number,
        y: number,
    },
    nickname: string,
    id: string,
}

interface IEvent {
    type: string,
    data: any,
}

class Game {
    config = {
        maxUsers: 8,
    };

    state = {
        players: new Map(),
    }

    constructor({ config }: IGame) {
        this.config = {
            ...this.config,
            ...config,
        };
    }

    start() {
        wss.on('connection', (ws: any) => {
            ws.id = wss.getUniqueID();

            ws.on('close', () => {
                this.onLeave(ws.id);
            })

            ws.on('message', (event: string) => {
                const { type, data }: IEvent = JSON.parse(event);
                console.log('received: %s', type, data);
                switch (type) {
                    case GameEvent.JOIN: {
                        this.onJoin({
                            ws,
                            id: ws.id,
                            nickname: data.nickname || ws.id,
                            position: { x: 0, y: 0 },
                            velocity: { x: 10, y: 10 },
                        })
                        break;
                    }

                    case PlayerEvent.UPDATE: {
                        console.log('data', ws.id, data);
                        this.onPlayerUpdate(ws.id, data);
                    }
                }
            });

        });
    }

    emitAll(type: string, data: any) {
        const players: Map<string, IPlayer> = this.state.players;

        players.forEach((player: IPlayer) => {
            const payload = JSON.stringify({
                type,
                data,
            });

            player.ws.send(payload)
        })
    }

    emit(ws: any, type: string, data: any) {
        const payload = JSON.stringify({
            type,
            data,
        });

        console.log(`Emit to ${ws.id} event - ${type}`)
        ws.send(payload)
    }

    onJoin(data: IPlayer) {
        this.state.players.set(data.id, data);

        this.emitAll(GameEvent.JOIN, data);
        this.emit(data.ws, GameEvent.LOGIN, data.id);
    }

    update() {
        this.emitAll(GameEvent.INIT, Array.from(this.state.players.values()));
        setTimeout(this.update.bind(this), 1000 / 5)
    }

    onLeave(id: string) {
        this.state.players.delete(id);
        this.emitAll(GameEvent.LEAVE, id);
    }

    onMove() {

    }

    onPlayerUpdate(id: string, data: any) {
        const player = this.state.players.get(id);
        if (!player) {
            return
        }

        this.state.players.set(id, {
            ...player,
            ...data,
        })
    }
}



app.get('/', function (req, res) {
    res.send('Unknown rpg api!!');
});

const wss = new WebSocket.Server({
    port: 8080,
    perMessageDeflate: {
        zlibDeflateOptions: {
            chunkSize: 1024,
            memLevel: 7,
            level: 3
        },
        zlibInflateOptions: {
            chunkSize: 10 * 1024
        },
        clientNoContextTakeover: true,
        serverNoContextTakeover: true,
        serverMaxWindowBits: 10,
        concurrencyLimit: 10,
        threshold: 1024
    }
});

wss.getUniqueID = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4();
};


const game = new Game({
    config: {
        maxUsers: 2,
    },
    wss,
})

game.start();
game.update();

const server = http.listen(3000, function () {
    console.log("listening on *:3000");
});