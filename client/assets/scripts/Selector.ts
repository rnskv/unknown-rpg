// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    object: cc.Node = null;

    @property(cc.Label)
    selector: cc.Label = null;

    onTouch(event: cc.Event.EventTouch) {
        event.stopPropagation();
        this.selector.string = 'ЦЕЛЬ'
        this.selector.node.x = this.object.x;
        this.selector.node.y = this.object.y;
    }

    start () {
        this.object.on(cc.Node.EventType.TOUCH_START, this.onTouch, this);
    }

    // update (dt) {}
}
