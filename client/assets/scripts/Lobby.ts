// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import Network  from '../core/Network';

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Button)
    button: cc.Button = null;

    @property(cc.EditBox)
    serverAddressField: cc.EditBox = null;

    @property(cc.Label)
    errorLabel: cc.Label = null;
    
    onTouchStart(event: cc.Event.EventTouch) {
        cc.tween(this.button.node).to(1000, { scale: 2 })
    }

    onTouchEnd(event: cc.Event.EventTouch) {
        let ws = new WebSocket(`ws://${this.serverAddressField.string}`);
        this.errorLabel.string = '';

        ws.onopen = function (event) {
            this.errorLabel.string = 'Подключение...';
            Network.setWS(ws);
            cc.director.loadScene("Battleground");

            setTimeout(() => {
                const data = JSON.stringify({ type: 'game:join', data: { nickname: 'rnskv' }});
                ws.send(data)
            }, 1000)
            
        }.bind(this);

        ws.onerror = function (event) {
            console.log("Send Text fired an error");
            this.errorLabel.string = 'Ошибка подключения';
        }.bind(this);

        ws.onclose = function (event) {
            console.log("WebSocket instance closed.");
            this.errorLabel.string = 'Не удалось подключиться';
        }.bind(this);
    }

    onLoad() {
        this.button.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.button.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    }
}
