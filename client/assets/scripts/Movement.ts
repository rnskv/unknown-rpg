const { ccclass, property } = cc._decorator;

interface IVector2 {
    x: number,
    y: number,
}

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Node)
    object: cc.Node = null;

    @property(cc.Camera)
    camera: cc.Camera = null;

    @property(cc.Node)
    canvas: cc.Node = null;

    @property
    destination: IVector2 = {
        x: 0,
        y: 0,
    }

    @property
    velocity: IVector2 = {
        x: 0,
        y: 0,
    };

    @property
    angle: number = 0;

    @property
    speed: number = null;

    onMouseDown(event: cc.Event.EventMouse) {
        console.log('Mouse down');
        const point = this.camera.getScreenToWorldPoint(event.getLocation());

        this.destination.x = point.x;
        this.destination.y = point.y;
    }

    onLoad() {
        this.canvas.on(cc.Node.EventType.TOUCH_START, this.onMouseDown, this);
    }

    move(dt) {
        const dx = this.destination.x - this.object.x;
        const dy = this.destination.y - this.object.y;
        const rb = this.object.getComponent(cc.RigidBody)

        this.angle = Math.atan2(dy, dx)
        this.velocity.x = this.speed * Math.cos(this.angle);
        this.velocity.y = this.speed * Math.sin(this.angle);
        this.object.angle = this.angle / Math.PI * 180 - 90;

        rb.linearVelocity = new cc.Vec2(this.velocity.x, this.velocity.y);
    }

    moveCamera(dt) {
        if (this.object.x > this.camera.node.width / 2) {
            this.camera.node.x = this.object.x;
        }

        if (this.object.y > this.camera.node.height / 2) {
            this.camera.node.y = this.object.y;
        }
    }

    update(dt) {
        this.move(dt);
        this.moveCamera(dt);
    }
}
