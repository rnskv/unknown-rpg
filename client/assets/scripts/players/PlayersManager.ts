const { ccclass, property } = cc._decorator;

@ccclass
export default class PlayersManager extends cc.Component {
    players = {};

    @property(cc.Prefab)
    playerPrefab: cc.Prefab = null;

    onLoad() {
        console.log('Init PlayersManager');
    }

    add(player: any) {
        const scene = cc.director.getScene();

        this.players[player.id] = cc.instantiate(this.playerPrefab);
        this.players[player.id].parent = scene;
        this.players[player.id].setPosition(0, 0);
    }

    patch(player) {
        console.log('patch player', player);
        if (!player.velocity) return;

        const rb = this.players[player.id].getComponent(cc.RigidBody);
        rb.linearVelocity = new cc.Vec2(player.velocity.x, player.velocity.y);
    }

    init(players) {
        players.forEach((player: any) => {
            if (!this.players[player.id]) {
                this.add(player)
            }

            this.patch(player);
        })
    } 

    join(player: any) {
        this.add(player)
        this.patch(player);
    }
}
