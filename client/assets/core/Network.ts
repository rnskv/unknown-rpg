// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

class Network {
    ws: any = null;

    setWS(ws: any) {
        this.ws = ws;
    }

    getWS() {
        return this.ws;
    }
}

export default new Network();