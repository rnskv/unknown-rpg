// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import network from './Network';

@ccclass
export default class NewClass extends cc.Component {
    @property
    isDebugMode: boolean = false;
    
    @property(cc.Component)
    playersManager: any = null;

    subscribe() {
        console.log('SUBSCRIVE', network);
        network.ws.onmessage = (event) => {
            console.log('onmessage', event)
            const { type, data } = JSON.parse(event.data);

            console.log(`Событие: ${type}`, data)

            switch (type) {
                case 'game:init': {
                    this.playersManager.init(data);
                }

                case 'game:join': {
                    this.playersManager.join(data);
                }

                // case 'game:action': {
                //     this.playersManager.action(player);
                // }

                // case 'game:leave': {
                //     this.playersManager.leave(player);
                // }
            }
        };
    }

    sync(data) {
        this.playersManager.sync(data.players)
    }

    onEnable() {
        const manager = cc.director.getPhysicsManager();
        manager.enabled = true;
        manager.debugDrawFlags = this.isDebugMode ? 1 : 0;

        this.subscribe();
    }
}
